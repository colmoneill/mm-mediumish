<?php
/**
 * ImageGallery Pro for Bludit
 * Template for a single album on a page
 * 
 * @package    Bludit
 * @subpackage ImageGallery Pro
 * @category   Plugins
 * @author     novafacile OÜ
 * @copyright  2022 by novafacile OÜ
 * @license    All rights reserved
 * @see        https://bludit-plugins.com
 * This program is distributed in the hope that it will be useful - WITHOUT ANY WARRANTY.
 */
?>

<!-- <?php if($images): ?>
  <?php if($albumInfo['title']): ?>
    <h3><?= $albumInfo['title'] ?></h3>
  <?php endif; ?> -->
  <div class="imagegallery">
    <?php foreach ($images as $image): ?>
      <div class="imagegallery-image">
        <a href="<?= $image['large']; ?>" class="imagegallery-image-link">
          <img src="<?= $image['thumbnail']; ?>">
        </a>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>