<!-- Begin Article
================================================== -->
<div class="container">
	<div class="row">

		<!-- Begin Post -->
		<div class="col-md-12 col-xs-12">
			<!-- Begin Cover Image -->
			<?php if ($page->coverImage()): ?>
			<div class="hero-image" style="background-image: url(<?php echo $page->coverImage();?>)">
			</div>
			<?php endif ?>
			<!-- End Cover Image -->
		</div>

		<div class="col-md-8 col-md-offset-2 col-xs-12">

			<!-- Load Bludit Plugins: Page Begin -->
			<?php Theme::plugins('pageBegin'); ?>

			<div class="mainheading">
				<h1 class="posttitle"><?php echo $page->title(); ?></h1>
				<div class="wrapfooter">
					<span class="author-meta">
					<span class="post-date"><?php echo $page->date(); ?></span><span class="dot"></span><span class="post-read"><?php echo $page->readingTime(); ?></span>
					</span>
				</div>
			</div>
		</div>

		<div class="col-md-12 gallery">
			<!-- Begin gallery -->
			<?php Theme::plugins('pageEnd'); ?>
			<!-- End galery -->	
		</div>

		<div class="col-md-8 col-md-offset-2 col-xs-12">
			<!-- Begin Post Content -->
			<div class="article-post">
				<?php echo $page->content(); ?>
			</div>
			<!-- End Post Content -->

			<!-- Begin Tags -->
			<?php if (!empty($page->tags(true))): ?>
			<div class="after-post-tags">
				<ul class="tags">
					<?php foreach ($page->tags(true) as $tagKey=>$tagName): ?>
					<li><a href="<?php echo DOMAIN_TAGS.$tagKey ?>"><?php echo $tagName; ?></a></li>
					<?php endforeach ?>
				</ul>
			</div>
			<?php endif; ?>
			<!-- End Tags -->

		</div>
		<!-- End Post -->

	</div>

	<!-- Begin Footer
	================================================== -->
	<div class="footer">
		<p class="pull-left">
			<?php echo $site->footer(); ?>
		</p>
		<p class="pull-right">
			<!-- Designed by <a target="_blank" href="https://www.wowthemes.net">Wowthemes</a> |
			Powered by <a target="_blank" href="https://www.bludit.com">Bludit.com</a> -->
		</p>
		<div class="clearfix">
		</div>
	</div>
	<!-- End Footer
	================================================== -->
</div>
<!-- End Article
================================================== -->
